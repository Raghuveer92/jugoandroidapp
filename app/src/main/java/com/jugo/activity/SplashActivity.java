package com.jugo.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.jugo.R;

import simplifii.framework.utility.Preferences;

public class SplashActivity extends AppBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Preferences.isUserLoggerIn()) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                } else
                    startNextActivity(LoginActivity.class);
                finish();
            }
        }, 3000);
    }
}
