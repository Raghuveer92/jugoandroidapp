package com.jugo.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.jugo.R;
import com.jugo.fragment.RegisterFragment;
import com.jugo.model.requests.SocialSignUp;
import com.jugo.model.requests.UserLoginRequest;
import com.jugo.model.response.LoginBaseApiResponse;
import com.jugo.model.response.pojo.LoginProfileData;
import com.jugo.util.ApiRequestGenerator;
import com.jugo.util.FireBaseFBLoginUtil;
import com.jugo.util.FireBaseGoogleLoginUtil;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class LoginActivity extends AppBaseActivity {

    private FireBaseGoogleLoginUtil fireBaseGoogleLoginUtil;
    private FireBaseFBLoginUtil fireBaseFBLoginUtil;

    private String username= "", password="", email="", name="", lastName="", accessToken="", profileImageUrl="";
    private Button btnSubmit;
    private int loginType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //printHash();
        fireBaseGoogleLoginUtil= FireBaseGoogleLoginUtil.getInstance(this, new FireBaseGoogleLoginUtil.GoogleSignInListener() {
            @Override
            public void onSuccess(GoogleSignInAccount account) {

                if (account != null){
                    if (account.getEmail() != null){
                        email= account.getEmail();
                    }
                    if (account.getGivenName() != null){
                        name= account.getGivenName();
                    }
                    if (account.getFamilyName() != null){
                        lastName = account.getFamilyName();

                    }
                    if (account.getPhotoUrl() != null){
                        Uri photoUrl = account.getPhotoUrl();
                        profileImageUrl = photoUrl.toString();
                    }
                    loginType = 3;
                    login(username, password, loginType, accessToken, name, lastName, email, profileImageUrl);
                }
                else{
                    showToast(getString(R.string.user_data));
                }

                /*startNextActivity(MainActivity.class);
                finish();*/
            }

            @Override
            public void onFailed() {

            }
        });
        fireBaseFBLoginUtil = new FireBaseFBLoginUtil(this, new FireBaseFBLoginUtil.FBLoginCallback() {
            @Override
            public void onSuccess(Bundle bundle) {

                if (bundle !=null){
                    loginType = 2;
                    if (bundle.getString("name") != null || bundle.getString("name") != ""||bundle.getString("name") != "null")
                        name = bundle.getString("name");
                    if (bundle.getString("last_name") != null || bundle.getString("last_name") != "" ||bundle.getString("last_name") != "null")
                        lastName = bundle.getString("last_name");
                    if (bundle.getString("email") != null || bundle.getString("email") != ""||bundle.getString("email") != "null")
                        email = bundle.getString("email");
                    if (bundle.getString("idFacebook") != null || bundle.getString("idFacebook") != ""||bundle.getString("idFacebook") != "null")
                        accessToken = bundle.getString("idFacebook");
                    if (bundle.getString("imageUrl") != null || bundle.getString("imageUrl") != ""||bundle.getString("imageUrl") != "null")
                        profileImageUrl = bundle.getString("imageUrl");

                    login(username, password, loginType, accessToken, name, lastName, email, profileImageUrl);
                }
                else{
                    showToast(getString(R.string.user_data));
                }
                /*startNextActivity(MainActivity.class);
                finish();*/
            }

            @Override
            public void onFailure() {

            }
        });
        setOnClickListener(R.id.btn_facebbok, R.id.btn_submit, R.id.tv_register, R.id.tv_forgot_password,
                R.id.btn_gplus);
    }



    public void toRegister(View view){
        addFragmentReplace(R.id.login_fragment_container, new RegisterFragment(), true);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.tv_register:
                toRegister(v);
                break;
            case R.id.btn_facebbok:
                fireBaseFBLoginUtil.initiateFbLogin();
                break;
            case R.id.btn_submit:
                startNextActivity(MainActivity.class);
                if(valid()){
                    loginType = 1;
                    login(username, password, loginType, accessToken, name, lastName, email, profileImageUrl);
                }
                break;
            case R.id.tv_forgot_password:
                break;
            case R.id.btn_gplus:
                fireBaseGoogleLoginUtil.login();
                break;
        }
    }
    public boolean valid(){

        boolean flag = true;
        username = getEditText(R.id.et_username);
        password = getEditText(R.id.et_password);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        if( username == null || TextUtils.isEmpty(username)){
            flag = false;
            showToast(getString(R.string.username_error_toast));
        }else if(TextUtils.isEmpty(password)){
            flag = false;
            showToast(getString(R.string.enter_password_error_toast));
        }
        return flag;
    }

    public void login(String username, String password, int loginType, String accessToken,
                      String name, String lastName, String email, String profileImageUrl){
        SocialSignUp socialSignUp;
        if (loginType == 1){
            socialSignUp= null;
        }
        else {
            socialSignUp = new SocialSignUp(accessToken, name, lastName, email, profileImageUrl);
        }
        UserLoginRequest userLoginRequest = new UserLoginRequest(username, password, loginType, socialSignUp);
        HttpParamObject httpParamObject = ApiRequestGenerator.loginRequest(userLoginRequest);
        executeTask(AppConstants.TASK_CODES.USER_LOGIN, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);

        if( response == null){
            showToast(getString(R.string.no_response));
            return;
        }
        if (taskCode == AppConstants.TASK_CODES.USER_LOGIN) {
            LoginBaseApiResponse loginResponse = (LoginBaseApiResponse) response;
            if (loginResponse.getError() == false) {
                LoginProfileData loginProfileData = loginResponse.getLoginProfileData();
                loginProfileData.save();
                showToast(getString(R.string.login_successfull));
                Preferences.saveData(Preferences.LOGIN_KEY, true);
                startNextActivity(MainActivity.class);
                finish();
            } else {
                showToast(loginResponse.getMessage());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK){
            return;
        }
        fireBaseGoogleLoginUtil.onActivityResult(requestCode,resultCode,data);
        fireBaseFBLoginUtil.onActivityResult(requestCode,resultCode,data);
    }

}

