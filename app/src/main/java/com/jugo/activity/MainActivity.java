package com.jugo.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import com.jugo.R;
import com.jugo.fragment.DrawerFragment;
import com.jugo.fragment.HomeFragment;
import com.jugo.listener.ChangeFragmentListner;
import java.util.ArrayList;

public class MainActivity extends AppBaseActivity implements DrawerLayout.DrawerListener, ChangeFragmentListner {
    private DrawerLayout drawerLayout;
    private FragmentManager fragmentManager;
    private boolean isDrawerOpen;
    public TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolBar("Home");
        drawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);
        drawerLayout.addDrawerListener(this);
        fragmentManager = getSupportFragmentManager();
        addDrawerFragment();
        addFragment(new HomeFragment(), false);
        //addFragment(SelectEventFragment.getInstance(this), false);
    }

    private void addDrawerFragment() {
        String[] val = getResources().getStringArray(R.array.drawer_items);
        ArrayList<String> arrayList = new ArrayList<>();
        for (String s : val) {
            arrayList.add(s);
        }
        DrawerFragment drawerFragment = DrawerFragment.getInstance(arrayList, this, drawerLayout);
        fragmentManager.beginTransaction().replace(R.id.main_navigation_view, drawerFragment).commit();
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.drawer_icon_three_lines;
    }

    @Override
    protected void onHomePressed() {
        drawerOperation();
    }
    private void drawerOperation() {
        if (isDrawerOpen) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            drawerLayout.openDrawer(Gravity.LEFT);
        }
    }

    @Override
    public void onBackPressed() {
        if (isDrawerOpen) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            isDrawerOpen = false;
            return;
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
            return;
        }
        super.onBackPressed();
    }


    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        isDrawerOpen = true;
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        isDrawerOpen = false;
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    @Override
    public void addFragment(Fragment fragment, boolean b) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (b) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.replace(R.id.fragment_container, fragment).commit();
    }

}
