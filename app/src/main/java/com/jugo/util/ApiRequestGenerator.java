package com.jugo.util;

import com.jugo.fragment.SelectDesignFragment;
import com.jugo.model.requests.CreateEventRequest;
import com.jugo.model.requests.UserLoginRequest;
import com.jugo.model.response.AllEventsResponse;
import com.jugo.model.response.LoginBaseApiResponse;
import com.jugo.model.requests.UserSignUpRequest;
import com.jugo.model.response.EventsDesignResponse;
import com.jugo.model.response.SelectEventsResponce;
import com.jugo.model.response.RegisterApiResponse;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;

/**
 * Created by rajnikant on 21/7/17.
 */

public class ApiRequestGenerator {

    public static HttpParamObject signupRequest(String fName, String lName, String email, String password) {
        UserSignUpRequest userSignUpRequest = new UserSignUpRequest(fName, lName, email, password);
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.USERS_SIGNUP);
        httpParamObject.setJson(JsonUtil.toJson(userSignUpRequest));
        httpParamObject.setPostMethod();
        httpParamObject.setClassType(RegisterApiResponse.class);
        httpParamObject.setJSONContentType();
        return httpParamObject;
    }

    public static HttpParamObject getAllEventsType() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_ALL_EVENTS_TYPE);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(SelectEventsResponce.class);
        return httpParamObject;
    }

    public static  HttpParamObject getDesign(int eventId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_DESIGN + eventId );
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(EventsDesignResponse.class);
        return httpParamObject;
    }
    public static HttpParamObject loginRequest(UserLoginRequest userLoginRequest) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.USERS_LOGIN);
        httpParamObject.setJson(JsonUtil.toJson(userLoginRequest));
        httpParamObject.setPostMethod();
        httpParamObject.setClassType(LoginBaseApiResponse.class);
        httpParamObject.setJSONContentType();

        return httpParamObject;

    }
    public static HttpParamObject createEventRequest(CreateEventRequest createEventRequest) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.EVENT_CREATE);
        httpParamObject.setJson(JsonUtil.toJson(createEventRequest));
        httpParamObject.setPostMethod();
        httpParamObject.setClassType(RegisterApiResponse.class);
        httpParamObject.setJSONContentType();
        return httpParamObject;

    }

    public static HttpParamObject getAllEvents() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_ALL_EVENTS);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(AllEventsResponse.class);
        return httpParamObject;

    }
}

