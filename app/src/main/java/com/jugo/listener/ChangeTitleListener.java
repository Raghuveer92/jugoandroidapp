package com.jugo.listener;

/**
 * Created by rajnikant on 12/7/17.
 */

public interface ChangeTitleListener {
    void setTitle(String title);
}
