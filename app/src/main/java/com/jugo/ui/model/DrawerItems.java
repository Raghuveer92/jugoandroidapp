package com.jugo.ui.model;

/**
 * Created by rajnikant on 12/7/17.
 */

public class DrawerItems {
    int itemImage;
    String itemName;
    public int getClickId() {
        return clickId;
    }

    public void setClickId(int clickId) {
        this.clickId = clickId;
    }

    int clickId = -1;

    public int getItemImage() {
        return itemImage;
    }

    public void setItemImage(int itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
