package com.jugo.model.response.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SelectEventsData {

@SerializedName("eventTemplateId")
@Expose
private Integer eventTemplateId;
@SerializedName("eventTemplateName")
@Expose
private String eventTemplateName;
@SerializedName("imageUrl")
@Expose
private String imageUrl;
@SerializedName("colorCode")
@Expose
private String colorCode;

public Integer getEventTemplateId() {
return eventTemplateId;
}

public void setEventTemplateId(Integer eventTemplateId) {
this.eventTemplateId = eventTemplateId;
}

public String getEventTemplateName() {
return eventTemplateName;
}

public void setEventTemplateName(String eventTemplateName) {
this.eventTemplateName = eventTemplateName;
}

public String getImageUrl() {
return imageUrl;
}

public void setImageUrl(String imageUrl) {
this.imageUrl = imageUrl;
}

public String getColorCode() {
return colorCode;
}

public void setColorCode(String colorCode) {
this.colorCode = colorCode;
}

}