package com.jugo.model.response.pojo;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

/**
 * Created by Prashant on 24-07-2017.
 */

public class LoginProfileData {
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("profileImgUrl")
    @Expose
    private String profileImgUrl;
    @SerializedName("userId")
    @Expose
    private int userId;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getProfileImgUrl() {
        return profileImgUrl;
    }

    public void setProfileImgUrl(String profileImgUrl) {
        this.profileImgUrl = profileImgUrl;
    }

    public void save(){
        Preferences.saveData(AppConstants.PREF_KEYS.USER_DATA, JsonUtil.toJson(this));
    }
    public static LoginProfileData getInstance(){
        String sessionJson = Preferences.getData(AppConstants.PREF_KEYS.USER_DATA, "");
        if(!TextUtils.isEmpty(sessionJson)){
            return new Gson().fromJson(sessionJson, LoginProfileData.class);
        }
        return null;
    }
}
