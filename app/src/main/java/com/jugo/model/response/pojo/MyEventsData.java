package com.jugo.model.response.pojo;

/**
 * Created by rajnikant on 15/7/17.
 */

public class MyEventsData {
    String ivEvent, eventTitle, eventDateTime, eventAddress;

    public String getIvEvent() {
        return ivEvent;
    }

    public void setIvEvent(String ivEvent) {
        this.ivEvent = ivEvent;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventDateTime() {
        return eventDateTime;
    }

    public void setEventDateTime(String eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    public String getEventAddress() {
        return eventAddress;
    }

    public void setEventAddress(String eventAddress) {
        this.eventAddress = eventAddress;
    }
}
