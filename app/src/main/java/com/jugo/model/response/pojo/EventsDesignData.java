package com.jugo.model.response.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Prashant on 25-07-2017.
 */

public class EventsDesignData {

    @SerializedName("designId")
    @Expose
    private int designId;
    @SerializedName("designImgUrl")
    @Expose
    private String designImgUrl;
    @SerializedName("eventTemplateId")
    @Expose
    private int eventTemplateId;

    public int getDesignId() {
        return designId;
    }

    public void setDesignId(int designId) {
        this.designId = designId;
    }

    public String getDesignImgUrl() {
        return designImgUrl;
    }

    public void setDesignImgUrl(String designImgUrl) {
        this.designImgUrl = designImgUrl;
    }

    public Integer getEventTemplateId() {
        return eventTemplateId;
    }

    public void setEventTemplateId(Integer eventTemplateId) {
        this.eventTemplateId = eventTemplateId;
    }
}
