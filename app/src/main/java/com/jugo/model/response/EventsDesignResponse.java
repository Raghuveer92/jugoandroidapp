package com.jugo.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jugo.model.response.pojo.EventsDesignData;

import java.util.List;

/**
 * Created by Prashant on 25-07-2017.
 */

public class EventsDesignResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<EventsDesignData> data = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<EventsDesignData> getData() {
        return data;
    }

    public void setData(List<EventsDesignData> data) {
        this.data = data;
    }

}
