package com.jugo.model.response.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FileUploadData {

@SerializedName("filePath")
@Expose
private String filePath;
@SerializedName("fileSize")
@Expose
private Integer fileSize;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Integer getFileSize() {
return fileSize;
}

public void setFileSize(Integer fileSize) {
this.fileSize = fileSize;
}

}