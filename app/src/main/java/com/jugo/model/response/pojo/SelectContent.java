package com.jugo.model.response.pojo;

import java.io.File;
import java.io.Serializable;

/**
 * Created by rajnikant on 18/7/17.
 */

public class SelectContent implements Serializable {
    private File file;
    private String fileType;
    private String filePath;
    private String mimetype;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }
}
