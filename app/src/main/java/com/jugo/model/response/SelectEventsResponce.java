package com.jugo.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jugo.model.response.pojo.SelectEventsData;

import java.util.List;

public class SelectEventsResponce {

@SerializedName("error")
@Expose
private Boolean error;
@SerializedName("code")
@Expose
private String code;
@SerializedName("message")
@Expose
private String message;
@SerializedName("data")
@Expose
private List<SelectEventsData> data = null;

public Boolean getError() {
return error;
}

public void setError(Boolean error) {
this.error = error;
}

public String getCode() {
return code;
}

public void setCode(String code) {
this.code = code;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public List<SelectEventsData> getData() {
return data;
}

public void setData(List<SelectEventsData> data) {
this.data = data;
}

}