package com.jugo.model.response.pojo;

/**
 * Created by rajnikant on 15/7/17.
 */

public class WishListData {
    String itemTitle, itemImage, itemSeller, itemPrice, itemDescription, itemProvider, itemPrime;
    boolean isPrime;

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemSeller() {
        return itemSeller;
    }

    public void setItemSeller(String itemSeller) {
        this.itemSeller = itemSeller;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemProvider() {
        return itemProvider;
    }

    public void setItemProvider(String itemProvider) {
        this.itemProvider = itemProvider;
    }

    public String getItemPrime() {
        return itemPrime;
    }

    public void setItemPrime(String itemPrime) {
        this.itemPrime = itemPrime;
    }

    public boolean isPrime() {
        return isPrime;
    }

    public void setPrime(boolean prime) {
        isPrime = prime;
    }
}
