package com.jugo.model.response.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AllEventsData {

@SerializedName("eventTitle")
@Expose
private String eventTitle;
@SerializedName("dateTime")
@Expose
private long dateTime;
@SerializedName("location")
@Expose
private String location;
@SerializedName("link")
@Expose
private String link;
@SerializedName("designId")
@Expose
private int designId;
@SerializedName("description")
@Expose
private String description;
@SerializedName("media")
@Expose
private String media;
@SerializedName("userId")
@Expose
private int userId;
@SerializedName("eventId")
@Expose
private int eventId;

public String getEventTitle() {
return eventTitle;
}

public void setEventTitle(String eventTitle) {
this.eventTitle = eventTitle;
}

public long getDateTime() {
return dateTime;
}

public void setDateTime(long dateTime) {
this.dateTime = dateTime;
}

public String getLocation() {
return location;
}

public void setLocation(String location) {
this.location = location;
}

public String getLink() {
return link;
}

public void setLink(String link) {
this.link = link;
}

public int getDesignId() {
return designId;
}

public void setDesignId(int designId) {
this.designId = designId;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getMedia() {
return media;
}

public void setMedia(String media) {
this.media = media;
}

public int getUserId() {
return userId;
}

public void setUserId(int userId) {
this.userId = userId;
}

public int getEventId() {
return eventId;
}

public void setEventId(int eventId) {
this.eventId = eventId;
}

public String getDateTimeString(){
    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm a");
    return simpleDateFormat.format(new Date(dateTime));
}

}