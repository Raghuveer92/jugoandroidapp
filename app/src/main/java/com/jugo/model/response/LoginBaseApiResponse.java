package com.jugo.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jugo.model.response.pojo.LoginProfileData;

/**
 * Created by Prashant on 24-07-2017.
 */

public class LoginBaseApiResponse {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private LoginProfileData loginProfileData;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginProfileData getLoginProfileData() {
        return loginProfileData;
    }

    public void setLoginProfileData(LoginProfileData loginProfileData) {
        this.loginProfileData = loginProfileData;
    }
}
