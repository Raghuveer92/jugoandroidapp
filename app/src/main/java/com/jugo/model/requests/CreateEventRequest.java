package com.jugo.model.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rajnikant on 27/7/17.
 */

public class CreateEventRequest {
    @SerializedName("eventTitle")
    @Expose
    private String eventTitle;
    @SerializedName("dateTime")
    @Expose
    private long dateTime;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("media")
    @Expose
    private String media;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("designId")
    @Expose
    private int designId;
    @SerializedName("userId")
    @Expose
    private int userId;

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDesignId() {
        return designId;
    }

    public void setDesignId(int designId) {
        this.designId = designId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    public CreateEventRequest(String eventTitle, long dateTime, String eventLocation, String eventLink, String eventMedia,
                              String desc1, int eventDesignId, int userId){
        this.eventTitle = eventTitle;
        this.dateTime = dateTime;
        this.location = eventLocation;
        this.link = eventLink;
        this.media = eventMedia;
        this.description = desc1;
        this.designId = eventDesignId;
        this.userId = userId;
    }
}
