package com.jugo.model.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLoginRequest {

@SerializedName("username")
@Expose
private String username;
@SerializedName("password")
@Expose
private String password;
@SerializedName("loginType")
@Expose
private int loginType;
@SerializedName("socialSignUp")
@Expose
private SocialSignUp socialSignUp;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getLoginType() {
return loginType;
}

public void setLoginType(int loginType) {
this.loginType = loginType;
}

public SocialSignUp getSocialSignUp() {
return socialSignUp;
}

public void setSocialSignUp(SocialSignUp socialSignUp) {
this.socialSignUp = socialSignUp;
}

    public  UserLoginRequest(String userName, String  password, int loginType, SocialSignUp socialSignUp){
        this.username=userName;
        this.password=password;
        this.loginType=loginType;
        this.socialSignUp=socialSignUp;
    }

}