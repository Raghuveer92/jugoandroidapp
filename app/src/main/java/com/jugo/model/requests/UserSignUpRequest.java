package com.jugo.model.requests;

/**
 * Created by rajnikant on 21/7/17.
 */

public class UserSignUpRequest {
    private String firstName, lastName, username, password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public  UserSignUpRequest(String firstName, String  lastName, String username, String password){
        this.firstName=firstName;
        this.lastName=lastName;
        this.username=username;
        this.password=password;
    }
}
