package com.jugo.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.jugo.R;
import com.jugo.activity.MainActivity;
import com.jugo.listener.ChangeFragmentListner;
import com.jugo.model.requests.CreateEventRequest;
import com.jugo.model.response.pojo.LoginProfileData;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.fragments.MediaFragment;

/**
 * Created by rajnikant on 12/7/17.
 */

public class EnterDetailsFragment extends AppBaseFragment {

    private ChangeFragmentListner changeFragmentListner;
    private String eventName, imageUrl;
    private String eventDate;
    private MediaFragment mediaFragment;
    private static final int FILE_SELECT_CODE = 0;
    private AddMediaFragment addMediaFragment;
    private static final int PLACE_PICKER_REQUEST = 1;
    private boolean permissionType;
    private int eventDesignId;
    private String eventMediaPath = null;

    public static EnterDetailsFragment getInstance(ChangeFragmentListner changeFragmentListner, String imageUrl, int eventDesignId) {
        EnterDetailsFragment enterDetailsFragment = new EnterDetailsFragment();
        enterDetailsFragment.changeFragmentListner = changeFragmentListner;
        enterDetailsFragment.imageUrl = imageUrl;
        enterDetailsFragment.eventDesignId = eventDesignId;
        return enterDetailsFragment;
    }

    @Override
    public void initViews() {
        initActivityToolBar(getString(R.string.enter_detail));
        mediaFragment = new MediaFragment();
        getFragmentManager().beginTransaction().add(mediaFragment, getString(R.string.media_fragment_added)).commit();
        setOnClickListener(R.id.ll_add_media, R.id.rl_preview, R.id.rl_create_wishlist, R.id.ll_date_time_pick, R.id.et_event_date_time,
                R.id.et_event_location, R.id.et_event_media);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ll_add_media:
                break;
            case R.id.rl_preview:
                validateAndPreview();
                break;
            case R.id.rl_create_wishlist:
                break;
            case R.id.ll_date_time_pick:
                setEventDate();
                break;
            case R.id.et_event_date_time:
                setEventDate();
                break;
            case R.id.et_event_location:
                permissionType = true;
                askPermission(permissionType);
                // placePicker();
                break;
            case R.id.et_event_media:
                permissionType = false;
                askPermission(permissionType);
                break;
        }
    }



    private void validateAndPreview() {
        if (TextUtils.isEmpty(getEditText(R.id.et_event_title))) {
            showToast(getString(R.string.toast_title));
        } else if (TextUtils.isEmpty(getEditText(R.id.et_event_date_time))) {
            showToast(getString(R.string.toast_event_date));
        } else if (TextUtils.isEmpty(getEditText(R.id.et_event_location))) {
            showToast(getString(R.string.toast_event_location));
        } else if (TextUtils.isEmpty(getEditText(R.id.et_event_media))) {
            showToast(getString(R.string.toast_event_media));
        } else if (TextUtils.isEmpty(getEditText(R.id.et_event_description))) {
            showToast(getString(R.string.toast_event_description));
        } else {
            String eventTitle = getEditText(R.id.et_event_title);
            String eventDateTime = getEditText(R.id.et_event_date_time);
            String eventLocation = getEditText(R.id.et_event_location);
            String eventLink = getEditText(R.id.et_event_link);
            String desc1 = getEditText(R.id.et_event_description);
            long dateTime = setDateTimeInMillis(eventDateTime);
            int userId = 0;
            LoginProfileData loginProfileData = LoginProfileData.getInstance();
            if (loginProfileData != null) {
                if (loginProfileData.getUserId() == 0) {

                } else {
                    userId = loginProfileData.getUserId();
                }

            }
            CreateEventRequest createEventRequest = new CreateEventRequest(eventTitle, dateTime, eventLocation,
                    eventLink, eventMediaPath, desc1, eventDesignId, userId);
            changeFragmentListner.addFragment(PreviewDesignFragment.getInstance(changeFragmentListner, createEventRequest, imageUrl, eventDateTime), true);
        }
    }

    private long setDateTimeInMillis(String eventDateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        Date date = null;
        try {
            date = sdf.parse(eventDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    private void placePicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void askPermission(final boolean permissionType) {
        PermissionListener permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                if (permissionType == false) {
                    addMediaFragment = AddMediaFragment.getInstance(new MediaFragment.MediaListener() {
                        @Override
                        public void setUri(Uri uri, String MediaType, String filePath) {
                            String fileName = filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length());
                            setText(fileName, R.id.et_event_media);
                            eventMediaPath = filePath;
                            addMediaFragment.dismiss();

                        }

                    });
                    getFragmentManager().beginTransaction().add(addMediaFragment, "AddMediaFragmentAdded").commit();

                } else {
                    placePicker();
                }
            }


            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }

        };
        new TedPermission(getActivity())
                .setPermissionListener(permissionListener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }


    private void setEventDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        final int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                        eventDate = simpleDateFormat.format(c.getTime());
                        setEventTime(c);

                    }

                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private void setEventTime(final Calendar c) {
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //eReminderTime.setText( hourOfDay + ":" + minute);
                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                c.set(Calendar.MINUTE, minute);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

                String time = simpleDateFormat.format(c.getTime());
                eventDate = eventDate + " " + time;
                setText(eventDate, R.id.et_event_date_time);
            }

        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PLACE_PICKER_REQUEST:
                if (data != null) {
                    Place selectedPlace = PlacePicker.getPlace(data, getActivity());
                    String location = (String) selectedPlace.getAddress();
                    setText(location, R.id.et_event_location);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_enter_details;
    }
}
