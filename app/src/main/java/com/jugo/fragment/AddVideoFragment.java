package com.jugo.fragment;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.TextView;

import com.jugo.R;

import simplifii.framework.fragments.MediaFragment;

//import com.esafirm.imagepicker.features.ImagePicker;

/**
 * Created by rajnikant on 18/7/17.
 */

public class AddVideoFragment extends BottomSheetDialogFragment {
    private MediaFragment cameraPicker, galleryPicker;
private MediaFragment.MediaListener mediaListener;
    public static AddVideoFragment getInstance(MediaFragment.MediaListener mediaListener) {
        AddVideoFragment addMediaFragment = new AddVideoFragment();
        addMediaFragment.mediaListener=mediaListener;
        return addMediaFragment;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.layout_add_image, null);
        dialog.setContentView(contentView);
        TextView tvCamera = (TextView) dialog.findViewById(R.id.btn_camera_upload);
        TextView tvGallery = (TextView) dialog.findViewById(R.id.btn_gallery_upload);
        cameraPicker= new MediaFragment();
        getFragmentManager().beginTransaction().add(cameraPicker, "videoPickerCamera").commit();
        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraPicker.getVideoFromCamera(mediaListener);
                dismiss();
            }
        });
        galleryPicker = new MediaFragment();
        getFragmentManager().beginTransaction().add(galleryPicker, "videoPickerGallery").commit();
        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryPicker.getVideoFromGallary(mediaListener);
                dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }
}

