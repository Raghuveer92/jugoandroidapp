package com.jugo.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jugo.R;
import com.jugo.listener.ChangeFragmentListner;
import com.jugo.model.response.AllEventsResponse;
import com.jugo.model.response.pojo.AllEventsData;
import com.jugo.util.ApiRequestGenerator;

import java.util.ArrayList;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by rajnikant on 15/7/17.
 */

public class EventsFragment extends AppBaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private ListView listView;
    private CustomListAdapter customListAdapter;
    private ArrayList<AllEventsData> allEventsDataList;
    private ChangeFragmentListner changeFragmentListner;


    public static EventsFragment getInstance(ChangeFragmentListner changeFragmentListner) {
        EventsFragment eventsFragment = new EventsFragment();
        eventsFragment.changeFragmentListner = changeFragmentListner;
        return eventsFragment;
    }

    @Override
    public void initViews() {
        initActivityToolBar(getString(R.string.events));
        getAllEventsData();
        listView = (ListView) findView(R.id.list_my_events);
        allEventsDataList = new ArrayList();
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_my_events, allEventsDataList, this);
        listView.setAdapter(customListAdapter);
        listView.setOnItemClickListener(this);

    }

    private void getAllEventsData() {
        /*int userId = 0;
        LoginProfileData loginProfileData = LoginProfileData.getInstance();
        if (loginProfileData != null) {
            if (loginProfileData.getUserId() == 0) {

            }
            else {
                userId=loginProfileData.getUserId();
            }
           }*/
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllEvents();
        executeTask(AppConstants.TASK_CODES.GET_ALL_EVENT, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.no_response));
            return;
        } else {
            switch (taskCode) {
                case AppConstants.TASK_CODES.GET_ALL_EVENT:
                    AllEventsResponse allEventsResponse = (AllEventsResponse) response;
                    if (allEventsResponse.getError() == false) {
                        if (allEventsResponse.getData() != null) {
                            allEventsDataList.addAll(allEventsResponse.getData());
                            customListAdapter.notifyDataSetChanged();
                        }

                    } else {
                        showToast(allEventsResponse.getMessage());
                    }
                    break;
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_my_events;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.llOptionEvent.setVisibility(View.GONE);
        holder.llShareEvent.setVisibility(View.VISIBLE);
        holder.tvViewWishList.setVisibility(View.VISIBLE);
        holder.tvViewWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragmentListner.addFragment(WishListFragment.getInstance(changeFragmentListner), true);
            }
        });
        AllEventsData allEventsData = allEventsDataList.get(position);
        if(allEventsData != null){
            if (allEventsData.getEventTitle() != null){
                holder.tvTitle.setText(allEventsData.getEventTitle());
            }
            if (allEventsData.getLocation() != null){
                holder.tvLocation.setText(allEventsData.getLocation());
            }
            holder.tvDateTime.setText(allEventsData.getDateTimeString());
        }
        // Picasso.with(getActivity()).load(myEventsData.getIvEvent()).into(holder.ivEvent);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    private class Holder {
        private ImageView ivEvent;
        private RelativeLayout rlShare;
        private TextView tvTitle, tvDateTime, tvLocation, tvViewWishList;
        private LinearLayout llOptionEvent, llShareEvent;

        public Holder(View view) {
            ivEvent = (ImageView) view.findViewById(R.id.iv_event_image);
//            rlShare = (RelativeLayout) view.findViewById(R.id.rl_share);
            tvTitle = (TextView) view.findViewById(R.id.tv_event_title);
            tvDateTime = (TextView) view.findViewById(R.id.tv_event_date_time);
            tvLocation = (TextView) view.findViewById(R.id.tv_event_location);
            llOptionEvent = (LinearLayout) view.findViewById(R.id.ll_event_option);
            llShareEvent = (LinearLayout) view.findViewById(R.id.ll_event_share);
            tvViewWishList = (TextView) view.findViewById(R.id.tv_view_wishlist);
        }
    }
}
