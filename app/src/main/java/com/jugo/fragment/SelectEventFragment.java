package com.jugo.fragment;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jugo.R;
import com.jugo.activity.MainActivity;
import com.jugo.listener.ChangeFragmentListner;
import com.jugo.model.response.pojo.SelectEventsData;
import com.jugo.model.response.SelectEventsResponce;
import com.jugo.util.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by rajnikant on 12/7/17.
 */

public class SelectEventFragment extends AppBaseFragment implements CustomListAdapterInterface {
    private GridView eventsGrid;
    private CustomListAdapter customListAdapter;
    private ChangeFragmentListner changeFragmentListner;
    private MainActivity mainActivity;
    private List<SelectEventsData> selectEventsDataList = new ArrayList();

    public static SelectEventFragment getInstance(ChangeFragmentListner changeFragmentListner) {
        SelectEventFragment selectEventFragment = new SelectEventFragment();
        selectEventFragment.changeFragmentListner = changeFragmentListner;
        return selectEventFragment;
    }
    @Override
    public void initViews() {
        initActivityToolBar(getString(R.string.select_event));
        eventsGrid = (GridView) findView(R.id.gv_select_event);
        TextView tv_emptyView= (TextView) findView(R.id.tv_empty_view);
        selectEventsDataList.clear();
        getAllEvents();
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_select_event, selectEventsDataList, this);
        eventsGrid.setAdapter(customListAdapter);
        customListAdapter.notifyDataSetChanged();
        eventsGrid.setEmptyView(tv_emptyView);
    }

    private void getAllEvents() {
        HttpParamObject httpParamObject = ApiRequestGenerator.getAllEventsType();
        executeTask(AppConstants.TASK_CODES.USER_SELECT_EVENT_TYPE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if( response == null){
            showToast(getString(R.string.no_response));
            return;
        }
        else {
            switch (taskCode){
                case AppConstants.TASK_CODES.USER_SELECT_EVENT_TYPE:
                    SelectEventsResponce selectEventsResponce = (SelectEventsResponce) response;
                        if(selectEventsResponce.getError() == false) {
                            selectEventsDataList.addAll(selectEventsResponce.getData());
                            customListAdapter.notifyDataSetChanged();
                        } else{
                            showToast(selectEventsResponce.getMessage());
                        }
                    break;
            }
        }

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_select_event;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final SelectEventsData selectEventsData = selectEventsDataList.get(position);
        if (selectEventsData != null){
            if (selectEventsData.getEventTemplateName()!=null){
                holder.tvEventText.setText(selectEventsData.getEventTemplateName());
            }
            if (selectEventsData.getColorCode() !=null){
                try {
                    holder.rlEventColor.setBackgroundColor(Color.parseColor(selectEventsData.getColorCode()));
                }
                catch (Exception e){
                    holder.rlEventColor.setBackgroundColor(Color.parseColor("#809fff"));
                }

            }
            if (selectEventsData.getImageUrl() != null){
                Picasso.with(getActivity()).load(selectEventsData.getImageUrl()).into(holder.ivEventImage);
            }
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.frameBorder.setVisibility(View.VISIBLE);
                if (selectEventsData.getEventTemplateId() != null){
                    int eventId = selectEventsData.getEventTemplateId();
                    changeFragmentListner.addFragment(SelectDesignFragment.getInstance(changeFragmentListner, eventId), true);
                }


            }
        });
        return convertView;
    }

    private class Holder {
    private ImageView ivEventImage;
    private FrameLayout frameBorder;
        private RelativeLayout rlEventColor;
        private TextView tvEventText;

    public Holder(View view) {
        ivEventImage = (ImageView) view.findViewById(R.id.iv_event_image);
        frameBorder = (FrameLayout) view.findViewById(R.id.fl_border);
        rlEventColor = (RelativeLayout) view.findViewById(R.id.rl_event_color);
        tvEventText = (TextView) view.findViewById(R.id.tv_event_name);
    }
}
}
