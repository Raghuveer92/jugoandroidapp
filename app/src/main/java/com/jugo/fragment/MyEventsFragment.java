package com.jugo.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jugo.R;
import com.jugo.model.response.pojo.MyEventsData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;

/**
 * Created by rajnikant on 15/7/17.
 */

public class MyEventsFragment extends AppBaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private ListView listView;
    private CustomListAdapter customListAdapter;
    private ArrayList<MyEventsData> myEventsDataList;

    public static MyEventsFragment getInstance() {
        MyEventsFragment myEventsFragment= new MyEventsFragment();
        return myEventsFragment;
    }

    @Override
    public void initViews() {
        initActivityToolBar("MY EVENTS");
        listView = (ListView) findView(R.id.list_my_events);
        myEventsDataList = new ArrayList();
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_my_events, myEventsDataList, this);
        listView.setAdapter(customListAdapter);
//        setOnClickListener(R.id.btn_load_more);
        setData();
        listView.setOnItemClickListener(this);

    }

    private void setData() {
        for(int i=0; i<10; i++){
            MyEventsData myEventsData = new MyEventsData();
            myEventsData.setEventTitle("Test123");
            myEventsData.setIvEvent("http://ecx.images-amazon.com/images/I/71A2pxQc3XL._UL1500_.jpg");
            myEventsDataList.add(myEventsData);
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_my_events;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        MyEventsData myEventsData = myEventsDataList.get(position);
        Picasso.with(getActivity()).load(myEventsData.getIvEvent()).into(holder.ivEvent);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
    private class Holder {
        private ImageView ivEvent;
        private RelativeLayout rlDelete, rlEdit, rlAdd, rlShare;
        private TextView tvTitle, tvDateTime, tvLocation;

        public Holder(View view) {
            ivEvent = (ImageView) view.findViewById(R.id.iv_event_image);
            /*rlDelete = (RelativeLayout) view.findViewById(R.id.rl_delete);
            rlEdit = (RelativeLayout) view.findViewById(R.id.rl_edit);
            rlAdd = (RelativeLayout) view.findViewById(R.id.rl_add);
            rlShare = (RelativeLayout) view.findViewById(R.id.rl_share);*/
            tvTitle = (TextView) view.findViewById(R.id.tv_event_title);
            tvDateTime = (TextView) view.findViewById(R.id.tv_event_date_time);
            tvLocation = (TextView) view.findViewById(R.id.tv_event_location);
        }
    }
}
