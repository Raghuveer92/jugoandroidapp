package com.jugo.fragment;

import android.Manifest;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.TextView;

//import com.esafirm.imagepicker.features.ImagePicker;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.jugo.R;

import java.util.ArrayList;

import simplifii.framework.fragments.MediaFragment;

/**
 * Created by rajnikant on 18/7/17.
 */

public class AddMediaFragment extends BottomSheetDialogFragment {
    private static GetMediaListener getMediaListener;
    private MediaFragment imagePicker, audioPicker, videoPicker, gifPicker;
private MediaFragment.MediaListener mediaListener;
    private boolean imageUpload;

    public static AddMediaFragment getInstance(MediaFragment.MediaListener mediaListener) {
        AddMediaFragment addMediaFragment= new AddMediaFragment();
        addMediaFragment.mediaListener=mediaListener;
        return addMediaFragment;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.layout_add_media, null);
        dialog.setContentView(contentView);

        TextView btn_image_upld = (TextView) dialog.findViewById(R.id.btn_image_upload);
        final TextView btn_audio_upld = (TextView) dialog.findViewById(R.id.btn_audio_upload);
        TextView btn_upld_video = (TextView) dialog.findViewById(R.id.btn_video_upload);
        TextView btn_gif_upload = (TextView) dialog.findViewById(R.id.btn_gif_upload);

        btn_image_upld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageUpload=true;
                askPermission();

            }
        });


        audioPicker = new MediaFragment();
        getChildFragmentManager().beginTransaction().add(audioPicker, "Audio Player").commit();
        btn_audio_upld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioPicker.getAudioFromPlayer(mediaListener);
                //dismiss();
            }
        });
        btn_upld_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageUpload=false;
                askPermission();

            }
        });
        gifPicker = new MediaFragment();
        getChildFragmentManager().beginTransaction().add(gifPicker, "GIF Files").commit();
        btn_gif_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gifPicker.getGifFromGallary(mediaListener);
                //dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    private void askPermission() {
        PermissionListener permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                //showDialogAddMedia();
                if(imageUpload == true){
                    AddImageFragment addImageFragment = AddImageFragment.getInstance(mediaListener);
                    getFragmentManager().beginTransaction().add(addImageFragment, "AddImageFragmentAdded").commit();
                    dismiss();
                }
                else{
                    AddVideoFragment addVideoFragment= AddVideoFragment.getInstance(mediaListener);
                    getFragmentManager().beginTransaction().add(addVideoFragment, "AddVideoFragmentAdded").commit();
                    dismiss();
                }
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }

        };
        new TedPermission(getActivity())
                .setPermissionListener(permissionListener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();
    }

    public interface GetMediaListener {
        public void imageData(Bundle bundle);

        public void videoData(Bundle bundle);

        public void audioData(Bundle bundle);

        public void pdfData(Bundle bundle);
    }
}
