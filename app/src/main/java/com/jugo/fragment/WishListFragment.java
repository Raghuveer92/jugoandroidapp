package com.jugo.fragment;

import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jugo.R;
import com.jugo.listener.ChangeFragmentListner;
import com.jugo.model.response.pojo.WishListData;

import java.util.ArrayList;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;

/**
 * Created by rajnikant on 15/7/17.
 */

public class WishListFragment extends AppBaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    private ListView listView;
    private CustomListAdapter customListAdapter;
    private ArrayList<WishListData> listWishListData;
    private ChangeFragmentListner changeFragmentListner;

    public static WishListFragment getInstance(ChangeFragmentListner changeFragmentListner) {
        WishListFragment wishListFragment = new WishListFragment();
        wishListFragment.changeFragmentListner = changeFragmentListner;
        return wishListFragment;
    }
    @Override
    public void initViews() {
        initActivityToolBar("WISHLIST");
        listView = (ListView) findView(R.id.list_wishlist);
        listWishListData = new ArrayList();
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_wish_list, listWishListData, this);
        listView.setAdapter(customListAdapter);
//        setOnClickListener(R.id.btn_load_more);
        setData();
        listView.setOnItemClickListener(this);

    }

    private void setData() {
        for(int i=0; i<10; i++){
            WishListData wishListData = new WishListData();
            wishListData.setItemDescription("test");
            wishListData.setItemTitle("mens cloths");
            wishListData.setItemPrice("14568");
           // wishListData.setItemImage("http://ecx.images-amazon.com/images/I/71A2pxQc3XL._UL1500_.jpg");
            listWishListData.add(wishListData);
//String.format(getString(R.string.rupees_symbol),
        }
        customListAdapter.notifyDataSetChanged();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_wish_list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        //String.format(getString(R.string.rupees_symbol))
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        WishListData wishListData = listWishListData.get(position);
        holder.rl_chqbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.rl_chqbox.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.bg_chekbox));
            }
        });
       // Picasso.with(getActivity()).load(wishListData.getItemImage()).into(holder.ivItemImage);
        //wishListData.setItemPrice(wishListData.get);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    private class Holder {
        private ImageView iv_prime, ivItemImage, iv_amazone;
        private RelativeLayout rl_chqbox;
        private TextView tvTitle, tvProvider, tvPrice, tvDescription, tvSavePrice;

        public Holder(View view) {
            iv_prime = (ImageView) view.findViewById(R.id.iv_prime);
            ivItemImage = (ImageView) view.findViewById(R.id.iv_item);
            iv_amazone = (ImageView) view.findViewById(R.id.iv_amazone);
            rl_chqbox = (RelativeLayout) view.findViewById(R.id.rl_checkbox);
            tvTitle = (TextView) view.findViewById(R.id.tv_item_title);
            tvProvider = (TextView) view.findViewById(R.id.tv_item_provider);
            tvPrice = (TextView) view.findViewById(R.id.tv_item_price);
            tvDescription = (TextView) view.findViewById(R.id.tv_item_description);
            tvSavePrice = (TextView) view.findViewById(R.id.tv_save_price);
        }
    }
}
