package com.jugo.fragment;

import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jugo.R;
import com.jugo.activity.LoginActivity;
import com.jugo.listener.ChangeFragmentListner;
import com.jugo.model.response.pojo.LoginProfileData;
import com.jugo.ui.model.DrawerItems;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by rajnikant on 12/7/17.
 */

public class DrawerFragment extends AppBaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    List<DrawerItems> itemsList = new ArrayList<>();
    ListView listDrawerItem;
    List<String> values = new ArrayList<>();
    private CustomListAdapter customListAdapter;
    private ChangeFragmentListner changeFragmentListner;
    DrawerLayout drawerLayout;
    private String[] name;

    public static DrawerFragment getInstance(List<String> arrayList, ChangeFragmentListner changeFragmentListner, DrawerLayout drawerLayout) {
        DrawerFragment drawerFragment = new DrawerFragment();
        drawerFragment.values = arrayList;
        drawerFragment.changeFragmentListner = changeFragmentListner;
        drawerFragment.drawerLayout = drawerLayout;
        return drawerFragment;
    }

    @Override
    public void initViews() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.header_drawer_main, null);
        setDataToDrawer(view);
        name = new String[]{"PROFILE",
                "CREATE EVENTS", "EVENTS",
                "MY EVENTS", "RATE US"};
        listDrawerItem = (ListView) findView(R.id.listDrawer);
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_drawer_name, itemsList, this);
        listDrawerItem.setAdapter(customListAdapter);
        listDrawerItem.addHeaderView(view);
        listDrawerItem.setOnItemClickListener(this);
        setOnClickListener(R.id.ll_main);
        setItemsInDrawer();

    }

    private void setDataToDrawer(View view) {
        TextView tv_ProfileName, tv_EditProfile, tv_Logout;
        CircleImageView profileImage = (CircleImageView) view.findViewById(R.id.iv_user_image);
        tv_ProfileName = (TextView) view.findViewById(R.id.tv_profile_name);
        tv_EditProfile = (TextView) view.findViewById(R.id.tv_edit_profile);
        tv_Logout = (TextView) view.findViewById(R.id.tv_logout);
        LoginProfileData loginProfileData = LoginProfileData.getInstance();
        if (loginProfileData != null) {
            if (loginProfileData.getFirstName() != null && loginProfileData.getLastName() != null) {
                String name = loginProfileData.getFirstName() + " " + loginProfileData.getLastName();
                tv_ProfileName.setText(name);
            }
            if (loginProfileData.getProfileImgUrl() != null) {
                Picasso.with(getActivity()).load(loginProfileData.getProfileImgUrl()).placeholder(R.mipmap.profile_photo).into(profileImage);
            }

        }
        tv_Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.deleteAllData();
                startNextActivity(LoginActivity.class);
                getActivity().finish();
            }
        });
        tv_EditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void setItemsInDrawer() {
        DrawerItems item1 = new DrawerItems();
        DrawerItems item2 = new DrawerItems();
        DrawerItems item3 = new DrawerItems();
        DrawerItems item4 = new DrawerItems();
        DrawerItems item5 = new DrawerItems();

        item1.setClickId(AppConstants.CLICK_ID.PROFILE);
        item2.setClickId(AppConstants.CLICK_ID.CREATE_EVENTS);
        item3.setClickId(AppConstants.CLICK_ID.EVENTS);
        item4.setClickId(AppConstants.CLICK_ID.MY_EVENTS);
        item5.setClickId(AppConstants.CLICK_ID.RATE);


        item1.setItemImage(R.mipmap.icon_profile_3x);
        item2.setItemImage(R.mipmap.icon_create_event_3x);
        item3.setItemImage(R.mipmap.icon_event_3x);
        item4.setItemImage(R.mipmap.icon_my_event_3x);
        item5.setItemImage(R.mipmap.icon_rate_3x);

        item1.setItemName(name[0]);
        item2.setItemName(name[1]);
        item3.setItemName(name[2]);
        item4.setItemName(name[3]);
        item5.setItemName(name[4]);

        itemsList.add(item1);
        itemsList.add(item2);
        itemsList.add(item3);
        itemsList.add(item4);
        itemsList.add(item5);

        customListAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ll_main:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        view.setSelected(true);
        customListAdapter.notifyDataSetChanged();
        if (position == 0) {
            return;
        }
        switch (itemsList.get(position - 1).getClickId()) {
            case AppConstants.CLICK_ID.PROFILE:
                /*WishListFragment wishListFragment = WishListFragment.getInstance();
                changeFragmentListner.addFragment(wishListFragment, true);
                drawerLayout.closeDrawers();*/
                break;
            case AppConstants.CLICK_ID.CREATE_EVENTS:
                SelectEventFragment selectEventFragment = SelectEventFragment.getInstance(changeFragmentListner);
                changeFragmentListner.addFragment(selectEventFragment, true);
                drawerLayout.closeDrawers();
                break;
            case AppConstants.CLICK_ID.RATE:
                break;
            case AppConstants.CLICK_ID.MY_EVENTS:
                MyEventsFragment myEventsFragment = MyEventsFragment.getInstance();
                changeFragmentListner.addFragment(myEventsFragment, true);
                drawerLayout.closeDrawers();
                break;
            case AppConstants.CLICK_ID.EVENTS:
                EventsFragment eventsFragment = EventsFragment.getInstance(changeFragmentListner);
                changeFragmentListner.addFragment(eventsFragment, true);
                drawerLayout.closeDrawers();
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        DrawerItems drawerItem = itemsList.get(position);
        holder.tv_side_drawer.setText(drawerItem.getItemName());
        holder.iv_side_drawer.setImageResource(drawerItem.getItemImage());
        return convertView;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_drawer;
    }

    class Holder {
        TextView tv_side_drawer;
        ImageView iv_side_drawer;

        public Holder(View view) {
            tv_side_drawer = (TextView) view.findViewById(R.id.tv_drawer_item);
            iv_side_drawer = (ImageView) view.findViewById(R.id.drawer_menu_icon);
        }
    }
}
