package com.jugo.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.jugo.R;
import com.jugo.listener.ChangeFragmentListner;
import com.jugo.model.response.pojo.EventsDesignData;
import com.jugo.model.response.EventsDesignResponse;
import com.jugo.util.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by rajnikant on 12/7/17.
 */

public class SelectDesignFragment extends AppBaseFragment implements CustomListAdapterInterface {
    private GridView eventsGrid;
    private CustomListAdapter customListAdapter;
    private int eventId;
    private ChangeFragmentListner changeFragmentListner;
    private int totalImages=4;
    private List<EventsDesignData> eventsDesignDataList = new ArrayList<>();

    public static SelectDesignFragment getInstance(ChangeFragmentListner changeFragmentListner, int eventId) {
        SelectDesignFragment selectDesignFragment = new SelectDesignFragment();
        selectDesignFragment.eventId = eventId;
        selectDesignFragment.changeFragmentListner = changeFragmentListner;
        return selectDesignFragment;
    }
    @Override
    public void initViews() {
        initActivityToolBar(getString(R.string.select_design));
        eventsDesignDataList.clear();
        getDesign();
        eventsGrid = (GridView) findView(R.id.gv_select_event);
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.row_select_design, eventsDesignDataList, this);
        eventsGrid.setAdapter(customListAdapter);
        setOnClickListener(R.id.btn_load_more);
       // eventsGrid.setOnItemClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_load_more:
                totalImages=totalImages+4;
                eventsDesignDataList.clear();
            break;
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_select_design;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final EventsDesignData eventsDesignData = eventsDesignDataList.get(position);
        if(eventsDesignData != null){
            if(eventsDesignData.getDesignImgUrl() != null){
                Picasso.with(getActivity()).load(eventsDesignData.getDesignImgUrl()).placeholder(R.mipmap.test_splash).into(holder.ivEventImage);
            }
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(eventsDesignData.getDesignImgUrl() != null)
                changeFragmentListner.addFragment(EnterDetailsFragment.getInstance(changeFragmentListner, eventsDesignData.getDesignImgUrl(), eventsDesignData.getDesignId()), true);
            }
        });
        return convertView;
    }

    private class Holder {
        private ImageView ivEventImage;

        public Holder(View view) {
            ivEventImage = (ImageView) view.findViewById(R.id.iv_design_image);
        }
    }

    public void getDesign(){
        HttpParamObject httpParamObject = ApiRequestGenerator.getDesign(this.eventId);
        executeTask(AppConstants.TASK_CODES.USER_GET_EVENT_DESIGN, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);

        if(response == null){
            showToast(getString(R.string.no_response));
        }
        else {
            switch (taskCode) {
                case AppConstants.TASK_CODES.USER_GET_EVENT_DESIGN:
                    EventsDesignResponse eventsDesignResponse = (EventsDesignResponse) response;
                        if (eventsDesignResponse.getError() == false) {
                            if (eventsDesignResponse.getData() != null) {
                                eventsDesignDataList.addAll(eventsDesignResponse.getData());
                                customListAdapter.notifyDataSetChanged();
                            } else {
                                showToast(getString(R.string.no_design_found));
                            }
                        }
                    else{
                            showToast(eventsDesignResponse.getMessage());
                        }
                    break;
            }
        }
    }
}
