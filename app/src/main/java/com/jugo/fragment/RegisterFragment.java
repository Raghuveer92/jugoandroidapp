package com.jugo.fragment;

import android.text.TextUtils;
import android.view.View;

import com.jugo.R;
import com.jugo.model.response.RegisterApiResponse;
import com.jugo.util.ApiRequestGenerator;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by rajnikant on 11/7/17.
 */

public class RegisterFragment extends AppBaseFragment {
    @Override
    public void initViews() {
        setOnClickListener(R.id.btn_submit);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_submit:
                validateAndSignUp();
                break;
        }
    }
    public void validateAndSignUp() {
        String fName = getEditText(R.id.et_fname);
        String lName= getEditText(R.id.et_lname);
        String email = getEditText(R.id.et_username);
        String password = getEditText(R.id.et_password);
        String cPassword = getEditText(R.id.et_cpassword);

        if (TextUtils.isEmpty(fName)) {
            showToast(getString(R.string.toast_name));
        } else if (TextUtils.isEmpty(lName)) {
            showToast(getString(R.string.toast_lname));
        } else if (TextUtils.isEmpty(email)) {
            showToast(getString(R.string.toast_email));
        } else if (TextUtils.isEmpty(email)) {
            showToast(getString(R.string.toast_valid_email));
        } else if (TextUtils.isEmpty(password)) {
            showToast(getString(R.string.toast_password));
        }
        else if (TextUtils.isEmpty(cPassword)) {
            showToast(getString(R.string.toast_cpassword));
        }
        else if (!password.equalsIgnoreCase(cPassword)) {
            showToast(getString(R.string.toast_password_mismatch));
        }
        else{
            HttpParamObject httpParamObject = ApiRequestGenerator.signupRequest(fName, lName, email, password);
            executeTask(simplifii.framework.utility.AppConstants.TASK_CODES.USER_SIGNUP, httpParamObject);
        }

    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast(getString(R.string.no_respponse));
            return;
        }
        if (taskCode == AppConstants.TASK_CODES.USER_SIGNUP) {
            RegisterApiResponse signupResponse = (RegisterApiResponse) response;
                if (signupResponse.getError() == false) {
                    showToast(getString(R.string.signup_sucessfully));
                    (getActivity()).getSupportFragmentManager().popBackStackImmediate();
                } else {
                        showToast(signupResponse.getMessage());
                }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_register;
    }
}
