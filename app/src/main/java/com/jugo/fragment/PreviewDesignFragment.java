package com.jugo.fragment;

import android.view.View;
import android.widget.ImageView;

import com.jugo.R;
import com.jugo.listener.ChangeFragmentListner;
import com.jugo.model.requests.CreateEventRequest;
import com.jugo.model.response.FileUploadResponce;
import com.jugo.model.response.pojo.FileUploadData;
import com.jugo.model.response.pojo.LoginProfileData;
import com.jugo.model.response.RegisterApiResponse;
import com.jugo.util.ApiRequestGenerator;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by rajnikant on 13/7/17.
 */

public class PreviewDesignFragment extends AppBaseFragment {

    private ChangeFragmentListner changeFragmentListner;
    private CreateEventRequest createEventRequest;
    private String imageUrl, eventDateTime;

    public static PreviewDesignFragment getInstance(ChangeFragmentListner changeFragmentListner, CreateEventRequest createEventRequest,
                                                    String imageUrl, String eventDateTime) {
        PreviewDesignFragment previewDesignFragment = new PreviewDesignFragment();
        previewDesignFragment.changeFragmentListner = changeFragmentListner;
        previewDesignFragment.createEventRequest = createEventRequest;
        previewDesignFragment.imageUrl = imageUrl;
        previewDesignFragment.eventDateTime = eventDateTime;
        return previewDesignFragment;
    }

    @Override
    public void initViews() {
        initActivityToolBar(getString(R.string.preview_design));
        setData();
        setOnClickListener(R.id.tv_create_event);


    }

    private void setData() {
        ImageView iv_background = (ImageView) findView(R.id.iv_background);
        Picasso.with(getActivity()).load(imageUrl).into(iv_background);
        setText(createEventRequest.getEventTitle(), R.id.tv_event_title);
        setText(eventDateTime, R.id.tv_event_date_time);
        setText(createEventRequest.getLocation(), R.id.tv_event_location);
        setText(createEventRequest.getDescription(), R.id.tv_event_desc2);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tv_create_event:
                uploadMedia();
                break;
        }
    }
    private void uploadMedia() {
        File file=new File(createEventRequest.getMedia());
        FileParamObject fileParamObject = new FileParamObject(file,file.getName(),"file");
        fileParamObject.setUrl(AppConstants.PAGE_URL.FILE_UPLOAD);
        fileParamObject.setClassType(FileUploadResponce.class);
        fileParamObject.setPostMethod();
        executeTask(AppConstants.TASK_CODES.USER_UPLOAD_FILE, fileParamObject);
    }

    private void createEvent(CreateEventRequest createEventRequest) {
        HttpParamObject httpParamObject = ApiRequestGenerator.createEventRequest(createEventRequest);
        executeTask(AppConstants.TASK_CODES.USER_CREATE_EVENT, httpParamObject);
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(getString(R.string.no_response));
        } else {
            switch (taskCode) {
                case AppConstants.TASK_CODES.USER_CREATE_EVENT:
                    RegisterApiResponse signupResponse = (RegisterApiResponse) response;
                    if (signupResponse.getError() == false) {
                        showToast(getString(R.string.event_create_sucessfully));
                        (getActivity()).getSupportFragmentManager().popBackStackImmediate();
                    } else {
                        showToast(signupResponse.getMessage());
                    }
                    break;
                case AppConstants.TASK_CODES.USER_UPLOAD_FILE:
                    FileUploadResponce fileUploadResponce = (FileUploadResponce) response;
                    if (fileUploadResponce.getError() == false) {
                        if (fileUploadResponce.getData() !=null){
                            FileUploadData data = fileUploadResponce.getData();
                            if (data !=null){
                                if (data.getFilePath() != null) {
                                    createEventRequest.setMedia(data.getFilePath());
                                    createEvent(createEventRequest);
                                }
                            }
                        }

                    }
                    else {
                        showToast(fileUploadResponce.getMessage());
                    }
                    break;
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_preview_designs;
    }
}
