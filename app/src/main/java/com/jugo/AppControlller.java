package com.jugo;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.Preference;

import simplifii.framework.utility.Preferences;

/**
 * Created by rajnikant on 19/7/17.
 */

public class AppControlller extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Preferences.initSharedPreferences(this);
    }
}
