package simplifii.framework.utility;

public interface AppConstants {

    String DEF_REGULAR_FONT = "OpenSans-Regular.ttf";
    String APP_LINK = "https://drive.google.com/file/d/0B8wKJnD6sONHeXlUbm5pOTk4dGM/view?usp=sharing";

    interface REQUEST_CODES {

        int GOOGLE_SIGHN_IN = 1;
    }


    public static interface PARAMS {
        String LAT = "latitude";
        String LNG = "longitude";
    }

    public static interface VALIDATIONS {
        String EMPTY = "empty";
        String EMAIL = "email";
        String MOBILE = "mobile";
    }

    public static interface ERROR_CODES {

        public static final int UNKNOWN_ERROR = 0;
        public static final int NO_INTERNET_ERROR = 1;
        public static final int NETWORK_SLOW_ERROR = 2;
        public static final int URL_INVALID = 3;
        public static final int DEVELOPMENT_ERROR = 4;

    }

    public static interface PAGE_URL {
        String BASE_URL = "http://192.168.1.14:8080/jugoapi";
        String USERS_LOGIN = BASE_URL+"/user/login";
        String USERS_SIGNUP = BASE_URL+"/user/signup";
        String GET_ALL_EVENTS_TYPE = BASE_URL+"/eventTemplates/";
        String GET_DESIGN = BASE_URL + "/design/?eventTemplateId=";
        String EVENT_CREATE = BASE_URL + "/event/create";
        String GET_ALL_EVENTS = BASE_URL +"/event/";
        String FILE_UPLOAD = BASE_URL + "/fileUpload";
    }

    public static interface PREF_KEYS {

        String KEY_LOGIN = "IsUserLoggedIn";
        String KEY_USERNAME = "username";
        String KEY_PASSWORD = "password";
        String ACCESS_CODE = "access";
        String APP_LINK = "appLink";
        String IS_LOGIN = "is_Login";
        String USER_DATA = "userData";
        String USER_KEYS = "userKeys";
    }

    public static interface BUNDLE_KEYS {
        public static final String KEY_SERIALIZABLE_OBJECT = "KEY_SERIALIZABLE_OBJECT";
        public static final String FRAGMENT_TYPE = "FRAGMENT_TYPE";
        String EXTRA_BUNDLE = "bundle";
    }

    public static interface FRAGMENT_TYPE {
        int HOME_FRAGMENT = 0;
        int TEAMS_FRAGMENT = 1;
        int ADD_TEAM_MATE = 2;
        int MY_TEAMS = 3;
        int MY_ORDERS = 4;
        int WEB_VIEW = 5;
    }

    public interface FILE_TYPES {

        String IMAGE = "image";
        String AUDIO = "audio";
        String VIDEO = "video";
        String GIF = "gif";
    }

    public interface TASK_CODES {

        int USER_SIGNUP = 1;
        int USER_LOGIN = 2;
        int UPLOAD_IMAGE = 3 ;
        int USER_SELECT_EVENT_TYPE = 4;
        int USER_GET_EVENT_DESIGN = 5;
        int USER_CREATE_EVENT = 6;
        int GET_ALL_EVENT = 7;
        int USER_UPLOAD_FILE = 8;
    }

    public static interface MEDIA_TYPES {
        String IMAGE = "img";
        String AUDIO = "audio";
        String VIDEO = "video";
        String GIF = "gif";
    }
    public interface CLICK_ID {
        int PROFILE = 1;
        int CREATE_EVENTS = 2;
        int EVENTS = 3;
        int MY_EVENTS = 4;
        int RATE = 5;
    }
    public interface JSON_FILES {
        String folder = "json/";
        String PAST_CURRENT_ILLNESS = folder + "past_current_illness.json";
        String ALLERGIES = folder + "allergies.json";
        String FURTHER_ANALIST = folder + "further_anylysis.json";
        String SYSTEM_REVIEW = folder + "system_review.json";
        String MEDICINE = folder + "medicine.json";
        String DIAGNOSIS = folder + "diagnosis.json";
        String QUALIFICATION = folder + "qualifications.json";
    }

}
